#xiaoshuocms
小说爬虫，web后台操作爬虫，ajax+bootstrap + django + urllib2 + mongodb
```
1.通过web后台输入参数爬取指定小说，可选爬取全部内容或只爬取小说章节标题，web端显示爬取进度（改为ajax异步显示实时爬取的章节名）
2.基于urllib2爬取小说内容，全部信息存于mongodb数据库
3.基于bootstrap画的小说阅读页和主页，主页显示数据库现在已经爬取的小说名(美工堪忧)
```